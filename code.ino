#include <ESP8266WiFi.h>
#include "polish_chars.h"

#define NETWORK_NAME "NAME"
#define NETWORK_PASS "PASS"
#define PINS  4

// #define DEBUG

int o[] = {13, 12, 14, 16};

WiFiServer server(80);

String prepareHtmlPage()
{
  String htmlPage = String(
"HTTP/1.1 200 OK\r\n" \
"Content-Type: text/html\r\n" \
"Connection: close\r\n" \
"\r\n" \
"<!doctype html>" \
"<html lang='pl'>" \
"  <head>" \
"    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>" \
"    <title>Przed" l_ "u" z_ "acz WiFi</title>" \
"    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>" \
"    <script>" \
"      function change(elem) {" \
"        const Http = new XMLHttpRequest();" \
"        const url='.?socket' + elem.id + '=' + elem.checked;" \
"        Http.open('GET', url);" \
"        Http.send();" \
"      }" \
"    </script>" \
"  </head>" \
"  <body>" \
"    <main role='main' class='container'>" \
"      <div class='card text-white bg-info mb-3' style='max-width: 18rem;'>" \
"        <div class='card-header'>Przed" l_ "u" z_ "acz WiFi</div>" \
"        <div class='card-body'>");
  for (int i=0; i < PINS; i++) {
    htmlPage += 
"<div class='custom-control custom-switch'>" \
"  <input type='checkbox' class='custom-control-input' id='" + String(i) + "' onClick='change(this)'" + (digitalRead(o[i]) ? " checked " : "") + ">" \
"  <label class='custom-control-label' for='" + String(i) + "'>Gniazdo " + String(i+1) + "</label>" \
"</div>";
  }
  htmlPage +=
"      </div>" \
"    </main>" \
"  </body>" \
"</html>" \
"\r\n\r\n";
  return htmlPage;
}

void setup() {
  for (int i=0; i < PINS; i++) {
    digitalWrite(o[i], LOW);
    pinMode(o[i], OUTPUT);
  }
#ifdef DEBUG
  Serial.begin(115200);
  Serial.println();
#endif /* DEBUG */
  WiFi.begin(NETWORK_NAME, NETWORK_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
#ifdef DEBUG
    Serial.print(".");
#endif /*DEBUG */
  }
#ifdef DEBUG
  Serial.print("\r\nConnected, IP address: ");
  Serial.println(WiFi.localIP());
#endif /* DEBUG */
  server.begin();
}

void loop() {
  WiFiClient client = server.available();
  if (client) {    
#ifdef DEBUG
    Serial.println("\r\n[Client connected]");
#endif /* DEBUG */
    while (client.connected()) {
      if (client.available()) {
        String line = client.readStringUntil('\n');        
#ifdef DEBUG
        Serial.print(line + '\n');
#endif /* DEBUG */
        if (line.length() == 1 && line[0] == '\r') {          
#ifdef DEBUG
          Serial.println("Sending...");
#endif /* DEBUG */
          client.print(prepareHtmlPage());
          break;
        }
        if (line.substring(6,12).equals("socket")) {
          int s = line.charAt(12) - '0';
          if (line.charAt(14) == 't')
            digitalWrite(o[s], HIGH);
          if (line.charAt(14) == 'f')
            digitalWrite(o[s], LOW);
#ifdef DEBUG
          Serial.println(String(s) + " is " + line.charAt(14));
#endif /* DEBUG */
        }
      }
    }
    delay(1);
    client.stop();
  }
}
