#ifndef polish_chars_h
#define polish_chars_h

#define a_ "&#261;"
#define A_ "&#260;"
#define c_ "&#263;"
#define C_ "&#262;"
#define e_ "&#281;"
#define E_ "&#280;"
#define l_ "&#322;"
#define L_ "&#321;"
#define n_ "&#324;"
#define N_ "&#323;"
#define o_ "&#243;"
#define O_ "&#211;"
#define s_ "&#347;"
#define S_ "&#346;"
#define z_ "&#380;"
#define Z_ "&#379;"
#define z__ "&#378;"
#define Z__ "&#377;"

#endif